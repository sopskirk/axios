(function($){
    $.fn.axios = function(config){
        var options = $.extend({
            type        : null,
            translate   : {},
        }, config);

        var type = isset(options.type, '');
    
        var $t = $(this);
        var $parent = $(this).parent();


        // Layout values
        var widthLayout     = null;
        var heightLayout    = null;
        var columnsLayout   = null;


        // Translate options
        var btnSaveText     = isset(options.translate.btnSaveText, 'Zapisz');
        var btnCancelText   = isset(options.translate.btnCancelText, 'Zamknij');
        var failConnect     = isset(options.translate.failConnect, 'Wystąpił błąd połączenia z serwerem!');
        var failDisconnect  = isset(options.translate.failDisconnect, 'Sprawdź swoje połączenie z internetem!');
        var unknowProblem   = isset(options.translate.unknowProblem, 'Wystąpił nieznany problem!');
        var textMuted       = isset(options.translate.textMuted, 'To pole jest wymagane');


        // Dialog options
        var nameDialog      = '';
        var shwowDialog     = false
        var toggleDialog    = false;
        var multiDialog     = false;
        var zIndexDialog    = 1050;
        var widthDialog     = 0;
        var heightDialog    = 0;
        var btnSaveName     = '.btn-save-data';

        var defaultValues = {
            nameDialog      : '',
            widthDialog     : 'bootbox-size-xl',
            heightDialog    : '500px',
            shwowDialog     : false,
            toggleDialog    : true,
            multiDialog     : false,
            zIndexDialog    : 1050,
            btnSaveName     : '.btn-save-data',
            titleDialog     : ''
        }

        var dialogSettings = {
            title       : 'Dialog title',
            message     : '<div class="kt-portlet" style="display:none;box-shadow:none;margin-bottom:0" id="GetAjaxData"></div>',
            size        : widthDialog,
            backdrop    : true,
            onEscape    : false,
            buttons     : {}
        }

    
        // Temp Ajax Data
        var loadSettings = {};
        var $dialog = null;


        // File upload
        var isImage = true;
        var $upload = null;


        const defaultValuesOfType = function(){
            switch (options.type) {
                case 'info' :
                    defaultValues = {
                        shwowDialog     : true,
                        toggleDialog    : false,
                        widthDialog     : 'bootbox-size-md',
                        heightDialog    : '565px'
                    }

                    dialogSettings.buttons = {
                        cancel: {
                            label       : btnCancelText,
                            className   : 'btn-secondary'
                        }
                    }
                break;
            }
        }


        /**
         * 
         * Creates a modal window using 
         * the bootbox library.
         * 
         * @return void
         * @package GetAjaxDataInfo
         * 
         */
        const dialog = function(){
            if ($('.bootbox').length == 0 || multiDialog) {
                bootbox.setDefaults({
                    locale      : 'pl',
                    show        : shwowDialog
                });

                $dialog = bootbox.dialog(dialogSettings);
                $dialog.find('div.modal-dialog').addClass(widthDialog).find('.bootbox-close-button').text('');

                if (!empty(nameDialog)) $dialog.addClass(nameDialog);

                if (multiDialog) {
                    $dialog.css('z-index', zIndexDialog);
                    $dialog.next($('.modal-backdrop')).css('z-index', ( zIndexDialog - 10 ));
                }
            }
        }


        /**
         * 
         * Creates layout for not modal version.
         * 
         * @return void
         * @package GetAjaxDataInfo
         * 
         */
        const layout = function(){
            if ($t.find('#GetAjaxData').length == 0 && type != 'sender') $t.append('<div id="GetAjaxData" style="max-width:' + widthLayout + '; max-height:' + heightLayout + '"></div>');
        }


        /**
         * 
         * Overwrites previously initialized data.
         * 
         * @return void
         * @package GetAjaxDataInfo
         * 
         */
        const overwrite = function(config){
            var settings = $.extend({
                dialog : {}
            }, config);

            shwowDialog  = isset(settings.dialog.show, shwowDialog);
            toggleDialog = isset(settings.dialog.toggle, toggleDialog);
        }


        /**
         * 
         * Trait for loading and manipulating data.
         * 
         * @param string type
         * @param data object
         * 
         * @return void
         * @package GetAjaxDataInfo
         * 
         */
        const getData = function(type, data){
            var i       = 0;
            var step    = 0;
            var modulo  = 0;
            var $view   = '';

            var $classess = {
                1 : {
                    label   : '',
                    input   : '',
                    group   : '',
                    portlet : 'padding:0'
                },

                2 : {
                    label   : 'col-lg-2 col-form-label',
                    input   : 'col-lg-4',
                    group   : 'form-group row form-group-marginless',
                    portlet : 'padding:0 25px'
                }
            }

            $classess = $classess[columnsLayout];

            switch (type) {
                // Inputs type
                case 'inputs' :
                    var $container = 
                        '<form method="POST" autocomplete="off" autocapitalize="off" class="datatable-modal-action kt-form kt-form--label-right">' +
                        '<div class="kt-scroll ps ps--active-y" data-scroll="true" style="max-height:' + heightDialog + ';overflow:hidden!important">' +
                        '<div class="kt-portlet__body GetAjaxDataForm" style="' + $classess.portlet + '"></div>' +
                        '</div><div class="kt-portlet__foot" style="padding-bottom:0;margin-top:25px"><div class="kt-form__actions">' +
                        '<div class="row"><div class="col-lg-9 ml-lg-auto">' +
                        '<button type="submit" class="btn btn-primary ' + btnSaveName.substr(1) + '">' + btnSaveText + '</button>'
                    ;

                    if (shwowDialog == true) $container += '<button type="reset" class="btn btn-secondary bootbox-cancel ml-3" data-dismiss="modal">' + btnCancelText + '</button>';
                    $container += '</div></div></div></div></form>';

                    $t.find('#GetAjaxData').prepend($container);
                    var $colorSelectors = {};
                    
                    $.each(data, function(key, value){
                        var $widget = isset(value.widget, '');
        
                        var $label          = isset(value.label, '');
                        var $select         = isset(value.select, '');
                        var $default        = isset(value.default, '');
                        var $required       = isset(value.required, false);
                        var $type           = isset(value.type, 'text');
                        var $keys           = isset(value.keys, false);
                        var $placeholder    = isset(value.placeholder, $label);
                        var $signature      = isset(value.signature, false);
                        var $maxlength      = isset(value.maxlength, 1000000);

                        var $dateFormat     = isset(value.format, $widget == 'Date' ? 'yyyy-mm-dd' : 'yyyy-mm-dd hh:ii');
                        var $viewModeDate   = isset(value.viewMode, 'days');

                        var accept = isset(value.accept, 'image.*');
        
                        modulo = i % columnsLayout;
                        
                        if (modulo == 0) {
                            step = i;

                            var classes = columnsLayout > 1 ? $classess.group + ' ' + (i == 0 ? 'kt-margin-t-20' : '') : $classess.group
                            $view += '<div class="' + classes + '">';
                        }
        
                        switch ($widget) {
                            case 'select' : case 'multiselect' :
                                $view += 
                                    '<label class="' + $classess.label + '">' + $label + ':</label>' +
                                    '<div class="' + $classess.input + '">'
                                ;

                                if ($widget == 'select') $view += '<select class="form-control ip-selectpicker data-field input-name-' + key + '" data-size="4" data-container="body" data-old-value="' + $default + '" name="' + key + '">';
                                else $view += '<select class="form-control kt-select2 ip-multiselectpicker data-field input-name-' + key + '" data-size="4" multiple="multiple" data-container="body" data-old-value="' + $default + '" name="' + key + '">';
            
                                var j = 0;
                                $.each($select, function(k, v){
                                    var selected    = $default == v || $default == $keys[j] ? 'selected' : '';
                                    var attr_value  = $keys != false ? $keys[j] : v;
                                    
                                    $view += '<option value="' + attr_value + '" ' + selected + '>' + v + '</option>';
                                    j++;
                                });
            
                                $view += '</select>';
                            break;
        
                            
                            case 'DateTime' :
                                $view +=
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<input ' +
                                        'type="text" ' +
                                        'class="form-control ip-datetimepicker data-field-date input-name-' + key + '" ' +
                                        'data-old-value="' + $default + '" ' +
                                        'data-required="' + $required + '" ' + 
                                        'required="' + $required + '" ' +
                                        'name="' + key + '" ' +
                                        'placeholder="' + $placeholder + '" ' +
                                        'data-date="' + $default + '"' +
                                        'data-date-format="' + $dateFormat + '"' +
                                        'data-view-mode="' + $viewModeDate + '"' +
                                    '/>'
                                ;
                            break;


                            case 'Date' :
                                $view +=
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<input ' +
                                        'type="text" ' +
                                        'class="form-control ip-datepicker data-field-date input-name-' + key + '" ' +
                                        'data-old-value="' + $default + '" ' +
                                        'data-required="' + $required + '" ' + 
                                        'required="' + $required + '" ' +
                                        'name="' + key + '" ' +
                                        'placeholder="' + $placeholder + '" ' +
                                        'data-date="' + $default + '"' +
                                        'data-date-format="' + $dateFormat + '"' +
                                        'data-view-mode="' + $viewModeDate + '"' +
                                    '/>'
                                ;
                            break;


                            case 'colorpicker' :
                                $colorSelectors = isset(value.colorSelectors, {});
                                
                                $view +=
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<input ' +
                                        'type="text" ' +
                                        'class="form-control ip-colorpicker data-field input-name-' + key + '" ' + 
                                        'data-old-value="' + $default + '" ' +
                                        'data-required="' + $required + '" ' + 
                                        'required="' + $required + '" ' +
                                        'name="' + key + '" ' +
                                        'placeholder="' + $placeholder + '" ' +
                                        'value="' + $default + '"' +
                                        'maxlength="' + $maxlength + '"' +
                                        'style="color:#fff;background:' + $default + '"' +
                                        'readonly' +
                                    '/>'
                                ;
                            break;


                            case 'filepicker' :
                                $view +=
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + ' custom-file">' +
                                    '<input ' +
                                        'type="file" ' +
                                        'class="form-control ip-filepicker custom-file-input data-file-input data-field input-name-' + key + '" ' +
                                        'data-old-value="' + $default + '"' +
                                        'data-required="' + $required + '" ' + 
                                        'required="' + $required + '" ' +
                                        'name="' + key + '" ' +
                                        'id="' + key + '"' +
                                        'accept="' + accept + '"' +
                                        'readonly ' +
                                        'style="opacity:0"' +
                                    '/>' +
                                    '<label class="custom-file-label" style="text-align:left" for="' + key + '">' + $placeholder + '</label>'
                                ;
                            break;


                            case 'textarea' :
                                $view +=
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<textarea ' +
                                        'class="form-control ip-maxlength data-field input-name-' + key + '"' +
                                        'placeholder="' + $placeholder + '"' +
                                        'data-old-value="' + $default + '"' +
                                        'data-required="' + $required + '"' +
                                        'required="' + $required + '"' +
                                        'maxlength="' + $maxlength + '"' +
                                        'name="' + key + '">' + $default +
                                    '</textarea>'
                                ;
                            break;


                            case 'wysiwyg' : 
                                $view += 
                                    '<label class="' + $classess.label + '">' + $label + '</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<div ' +
                                        'style="display:none"' +
                                        'class="summernote data-field" ' +
                                        'data-required="' + $required + '" ' +
                                        'required="' + $required + '"' +
                                    '>' + $default + '</div>'
                                ;
                            break;


                            default :
                                $view += 
                                    '<label class="' + $classess.label + '">' + $label + ':</label>' +
                                    '<div class="' + $classess.input + '">' +
                                    '<input ' + 
                                        'type="' + $type + '" ' +
                                        'class="form-control ip-maxlength data-field input-name-' + key + '" ' +
                                        'data-old-value="' + $default + '" ' +
                                        'data-required="' + $required + '" ' + 
                                        'required="' + $required + '" ' +
                                        'name="' + key + '" ' +
                                        'placeholder="' + $placeholder + '" ' +
                                        'value="' + $default + '"' +
                                        'maxlength="' + $maxlength + '"' +
                                    '/>'
                                ;
                            break;
                        }

                        if ($required == true || $signature != false) $view += '<span class="form-text text-muted" style="font-size:11px">' + isset(value.signature, textMuted) + '</span>';
                        $view += '</div>';
        
                        if ((step + (columnsLayout - 1)) == i || data.length == i) $view += '</div><div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>';
                        i++;
                    });

                    $t.find('.GetAjaxDataForm').append($view);
                    const $GetAjaxDataForm = $t.find('.GetAjaxDataForm');

                    $GetAjaxDataForm.find('.kt-separator:last-child').remove();

                    const $selectpicker         = $GetAjaxDataForm.find('select.ip-selectpicker');
                    const $multiselectpicker    = $GetAjaxDataForm.find('select.ip-multiselectpicker');
                    const $datepicker           = $GetAjaxDataForm.find('input.ip-datepicker');
                    const $datetimepicker       = $GetAjaxDataForm.find('input.ip-datetimepicker');
                    const $colorpicker          = $GetAjaxDataForm.find('input.ip-colorpicker');
                    const $filepicker           = $GetAjaxDataForm.find('input.ip-filepicker');
                    const $maxlength            = $GetAjaxDataForm.find('input.ip-maxlength');
                    const $textarea             = $GetAjaxDataForm.find('textarea');
                    const $wysiwyg              = $GetAjaxDataForm.find('.summernote');
        
                    if ($selectpicker.length > 0) {
                        $selectpicker.each(function(){
                            $(this).selectpicker();
                        });
                    }

                    if ($multiselectpicker.length > 0) {
                        $multiselectpicker.each(function(){
                            $(this).select2();
                        });
                    }
                    
                    if ($datepicker.length > 0) {
                        $datepicker.each(function(){
                            $(this).DatePicker();
                        });
                    }
                    
                    if ($datetimepicker.length > 0) {
                        $datetimepicker.each(function(){
                            $(this).DateTimePicker();
                        });
                    }

                    if ($filepicker.length > 0) {
                        $upload = $filepicker;
                        
                        $upload.each(function(){
                            isImage = $(this).FilePicker();
                            if (!isImage) break;
                        });
                    }
                    
                    if ($colorpicker.length > 0) {
                        $colorpicker.colorpicker({
                            colorSelectors  : $colorSelectors,
                            format          : 'hex'
                        });

                        $colorpicker.on('changeColor', function(e){
                            $colorpicker.css('background', e.value);
                        });
                    }

                    if ($maxlength.length > 0) {
                        $maxlength.maxlength({
                            alwaysShow          : true,
                            threshold           : 5,
                            placement           : 'top',
                            warningClass        : 'kt-badge kt-badge--primary kt-badge--rounded kt-badge--inline',
                            limitReachedClass   : 'kt-badge kt-badge--brand kt-badge--rounded kt-badge--inline',
                            appendToParent      : true
                        });
                    }
                    
                    if ($textarea.length > 0) {
                        autosize($textarea);
                        autosize.update($textarea);
                    }

                    if ($wysiwyg.length > 0) {
                        $wysiwyg.summernote({ height : 150 });
                        $wysiwyg.summernote('reset');

                        $wysiwyg.on('summernote.keyup', function(we, e) {
                            if (e.which != 9) $wysiwyg.next('.note-editor').css('border-color', '#ebedf2');
                        });
                    }
                    
                    $GetAjaxDataForm.find('.data-field').on('keyup', function(e){
                        if (e.which != 9) $(this).removeClass('is-invalid');
                    });
                    
                    $GetAjaxDataForm.parents('form').find('.kt-portlet__foot').show();
                break;


                // Text type
                case 'text' :
                    $t.find('#GetAjaxData').append('<div id="GetAjaxDataText" style="padding:25px"></div>');
                    
                    $.each(data, function(key, value){
                        var $default    = isset(value.default, '');
                        var $label      = isset(value.label, '');

                        modulo = i % columnsLayout;
                        
                        if (modulo == 0) {
                            step = i;
                            $view += '<div class="kt-widget12__item">';
                        }

                        $view += 
                            '<div class="kt-widget12__info">' +
                            '<span class="kt-widget12__desc text-uppercase" style="font-size:0.9rem!important">' + $label + '</span>' +
                            '<span style="font-size:1rem!important">' + $default  + '</span>' +
                            '</div>'
                        ;

                        if ((step + (columnsLayout - 1)) == i || data.length == i) $view += '</div>';
                        
                        i++;
                    });

                    $t.find('#GetAjaxDataText').append($view);
                break;


                // Info type
                case 'info' :
                    var $view =
                        '<div class="kt-scroll ps ps--active-y" data-scroll="true" style="max-height: ' + heightDialog + ';overflow: hidden!important" id="GetAjaxDataInfo">' +
                        '<ul class="list-info-about"></ul>' +
                        '</div>'
                    ;

                    $t.find('#GetAjaxData').prepend($view);

                    var $view = '';
                    $.each(data, function(key, value){
                        if (value !== null) {
                            $view += 
                                '<li><div class="list-info-about--header">' +
                                '<span class="kt-font-bold kt-font-dark">' + value.label + '</span></div>' +
                                '<div class="list-info-about--content"><span>' + value.default + '</span></div></li>'
                            ;
                        }
                    });

                    $t.find('#GetAjaxDataInfo ul').append($view);
                break;
            }

            if ($('#GetAjaxData .kt-scroll').length > 0) {
                var ps = new PerfectScrollbar('#GetAjaxData .kt-scroll', {
                    wheelSpeed          : 0.5,
                    swipeEasing         : true,
                    wheelPropagation    : false,
                    minScrollbarLength  : 40,
                    maxScrollbarLength  : 300,
                    useBothWheelAxes    : false,
                    suppressScrollX     : true,
                    includePadding      : true
                });

                ps.update();
            }
        }


        /**
         * 
         * It loads all data 
         * and operations on them.
         * 
         * @return void
         * @package GetAjaxDataInfo
         * 
         */
        const load = function(config){
            defaultValuesOfType();

            var settings = $.extend({
                saveButton  : {},
                dialog      : {},
                source      : {},
                layout      : {}
            }, config);

            columnsLayout = settings.layout.columns;

            // Set dialog options
            nameDialog      = isset(settings.dialog.name, defaultValues.nameDialog);
            shwowDialog     = isset(settings.dialog.show, defaultValues.shwowDialog);
            toggleDialog    = isset(settings.dialog.toggle, defaultValues.toggleDialog);
            multiDialog     = isset(settings.dialog.multi, defaultValues.multiDialog);
            zIndexDialog    = isset(settings.dialog.zIndex, defaultValues.zIndexDialog);
            widthDialog     = isset(settings.dialog.width, defaultValues.widthDialog);
            heightDialog    = isset(settings.dialog.height, defaultValues.heightDialog);
            titleDialog     = isset(settings.dialog.title, defaultValues.titleDialog);
            btnSaveName     = isset(settings.saveButton.name, defaultValues.btnSaveName);

            // Default source data
            var method      = isset(settings.source.method, 'POST');
            var cache       = isset(settings.source.cache, true);
            var processData = isset(settings.source.processData, true);
            var contentType = isset(settings.source.contentType, 'application/x-www-form-urlencoded;charset=UTF-8');
            var dataType    = isset(settings.source.dataType, 'json');
            var getJSON     = isset(settings.json, false);

            var getStatusResponse = isset(settings.source.getStatusResponse, false);

            if (shwowDialog == true) {
                dialog();
                
                $t = $parent = $dialog;
                $t.find('.modal-title').text(titleDialog);
            } 
            
            else layout();

            const $GetAjaxData = $t.find('#GetAjaxData');

            $GetAjaxData.hide().empty();
            resetScrollabe($t.find('.kt-scroll'));

            if (!getJSON) {
                loadSettings = {
                    saveButton : {
                        name    : btnSaveName
                    },
                   
                    dialog   : {
                        name    : nameDialog,
                        show    : shwowDialog,
                        toggle  : toggleDialog,
                        multi   : multiDialog,
                        zIndex  : zIndexDialog,
                        title   : titleDialog,
                        width   : widthDialog,
                        height  : heightDialog
                    },
                    
                    source  : {
                        getStatusResponse : getStatusResponse,
                        url         : settings.source.url,
                        method      : method,
                        contentType : contentType,
                        processData : processData,
                        cache       : cache,
                        dataType    : dataType,
                        data        : settings.source.data,
                        before      : settings.source.before,
                        done        : settings.source.done,
                        fail        : settings.source.fail
                    },

                    layout  : {
                        columns : columnsLayout
                    },

                    json    : getJSON
                };
                
                $.ajax({
                    url         : settings.source.url,
                    method      : method,
                    contentType : contentType,
                    processData : processData,
                    cache       : cache,
                    dataType    : dataType,
                    data        : settings.source.data,

                    beforeSend  : function(){
                        var $preloader = 
                            '<div class="LoadAjaxData kt--custom-preloader-data-2">' +
                            '<div class="kt-spinner kt-spinner--md kt-spinner--info">' +
                            '</div></div>'
                        ;

                        shwowDialog == true ? $t.find('.modal-body').prepend($preloader) : $parent.prepend($preloader);

                        // Dodatkowe dzialania
                        if (settings.source.before && typeof(settings.source.before) === 'function') return settings.source.before.call(this);
                    }
                })

                .done(function(response){
                    $done = true;

                    $parent.find('.LoadAjaxData').fadeOut(300, function(){
                        $(this).remove();
                        $GetAjaxData.fadeIn(300);
                    });

                    // Sukces
                    if (response.status == true || response.status == null) {
                        if (response.status == true) {
                            getData(type, response.data);

                            // Dodatkowe dzialania
                            if (settings.source.done && typeof(settings.source.done) === 'function') return settings.source.done.call(this, (!getStatusResponse ? response.data : response));
                        }
                        
                        else {
                            $GetAjaxData.prepend('<div class="none-data"><p style="margin:0;padding:0">Brak rekordów do wyświetlenia.</p></div>');
                            $GetAjaxData.parents('form').find('.kt-portlet__foot').hide();
                        }
                    }

                    // Sesja wygasła
                    else if (response.status == (-10)) cswal('Twoja sesja wygasła!', 'Musisz ponownie się zalogować, aby kontynuuować operację.', 'error').then(function(){ location.reload() });
                                
                    // Błąd
                    else {
                        response.error == undefined ? swal.fire('Nieznany błąd!', unknowProblem, 'error') : swal.fire('Wystąpił błąd', response.error, 'error');

                        // Dodatkowe dzialania
                        if (settings.source.fail && typeof(settings.source.fail) === 'function') return settings.source.fail.call(this);
                    }
                })

                .fail(function(){
                    $parent.find('.LoadAjaxData').fadeOut(300, function(){
                        $(this).remove();
                        $GetAjaxData.fadeIn(300);
                    });

                    swal.fire('Błąd połączenia!', navigator.onLine ? failConnect : failDisconnect, 'error');

                    // Dodatkowe dzialania
                    if (settings.source.fail && typeof(settings.source.fail) === 'function') return settings.source.fail.call(this);
                });
            } else {
                $GetAjaxData.show();
                getData(type, getJSON);
            }
        }


        /**
         * 
         * Overloading the load method.
         * 
         * @return void
         * @package axios
         * 
         */
        const reload = function(){
            load(loadSettings);
        }


        /**
         * 
         * Sets of new parameters 
         * to the load method.
         * 
         * @param object config
         * @return void
         * 
         * @package axios
         * @access public
         * 
         */
        const params = function(config){
            for (key in config) loadSettings.source.data[key] = config[key];
            reload();
        }


        /**
         * 
         * Generates source data for multi modal.
         * 
         * @param object data
         * @return object
         * 
         * @package axios
         * @access private
         * 
         */
        const sourceDataMultiModal = function(data){
            var $data = data;
            var $sourceData = {};
                                    
            for (key in $data) {
                if (key != 'columns') $sourceData[key] = $data[key];
            }

            if ($data.columns != undefined) {
                $sourceData['columns'] = {};

                $.each($data.columns, function(col, data){
                    var $input = '.input-name-' + col;

                    if (data.widget == 'select') $input += ' option:selected';
                    $sourceData['columns'][col] = $($input).val();
                });
            }

            return $sourceData;
        }


        /**
         * 
         * He will use the changed 
         * data and send it using AJAX.
         * 
         * @param object config
         * @return void
         * 
         * @package axios
         * @access public
         * 
         */
        const save = function(config){
            var settings = $.extend({
                options     : {},
                button      : {},
                source      : {},
                translate   : {},
                operation   : {},
                swal        : {}
            }, config);

            var $btn = $(btnSaveName);

            // Dodatkowe dzialania dla przycisku
            if (settings.button.callback && typeof(settings.button.callback) === 'function') settings.button.callback.call(this, e);

            const $fields = $t.find('.GetAjaxDataForm').find('input.data-field, select.data-field option:selected, textarea.data-field, input.data-field-date, div.data-field');
            var operation = isset(settings.operation, 'edit');
            
            // Default values
            var confirmOperation    = isset(settings.translate.confirmOperation, 'Czy na pewno chcesz to zrobić?');
            var successOperation    = isset(settings.translate.successOperation, 'Operacja została pomyślnie wykonana!');


            // Default options data
            var statusAllowed = isset(settings.options.statusAllowed, [ true ]);
            

            // Default swal data
            var swalTitle  = isset(settings.swal.title, 'Swal title');
            var swalText   = isset(settings.swal.text, 'Chcesz wysłać niezmienione dane!');
            var swalType   = isset(settings.swal.type, 'info');


            // Default source data
            var method      = isset(settings.source.method, 'POST');
            var contentType = isset(settings.source.contentType, 'application/x-www-form-urlencoded;charset=UTF-8');
            var dataType    = isset(settings.source.dataType, 'json');
            var cache       = isset(settings.source.cache, true);
            var processData = isset(settings.source.processData, true);

            const isSendFile = contentType != 'application/x-www-form-urlencoded;charset=UTF-8' ? true : false;

            swal.fire({
                title   : swalTitle,
                text    : confirmOperation,
                type    : 'warning',
                
                showCancelButton    : true,
                cancelButtonText    : 'Nie',
                confirmButtonText   : 'Tak',

                allowOutsideClick : false
            }).then(function(result) {
                if (result.value) {
                    var ok = false;
                    var invalid = false;
                    var isWysiwyg = false;

                    $fields.each(function(){
                        var $input  = $(this);
                        var $type   = $input.prop('tagName');

                        if ($input.hasClass('summernote')) {
                            var value = $('.summernote').summernote('code');
                            isWysiwyg = true;
                        }
                        
                        else var value = $input.hasClass('data-field-date') ? $input.data('date') : $input.val();

                        if ($input.hasClass('data-file-input')) {
                            var $files = $upload[0].files[0];
                            value = $files != undefined ? $files.name : $input.data('old-value');
                        }

                        if (operation == 'edit') {
                            var old_value = $type == 'OPTION' ? $input.parents('select').data('old-value') : $input.data('old-value');

                            if ((value != old_value) && !empty(value)) {
                                ok = true;
                                return false;
                            }
                        } else {
                            if ($input.data('required') == true && empty(value)) {
                                if (isWysiwyg) $input.next('.note-editor').css('border-color', '#fd27eb');
                                
                                $input.addClass('is-invalid');
                                invalid = true;
                            }

                            ok = !invalid;
                        }
                    });

                    if (ok && operation != 'edit') ok = isSendFile ? isImage : ok;

                    if (ok) {
                        var $sourceData = settings.source.data;
                        const $fields = $t.find('.GetAjaxDataForm').find('input.data-field, select.data-field, textarea.data-field, input.data-field-date').serializeArray();
                        
                        // Tworzy obiekt FormData
                        if (isSendFile) {
                            var $formData = new FormData();
                            $formData.append('upload', $upload[0].files[0]);
                            
                            for (key in $sourceData) $formData.append(key, $sourceData[key]);

                            $($fields).each(function(i, field){
                                $formData.append('fields[' + field.name + ']', field.value);
                            });
                        }

                        else {
                            $sourceData.fields = $fields;
                            if (isWysiwyg) $sourceData.wysiwyg = $('.summernote').summernote('code');

                            $formData = $sourceData;
                        }

                        $.ajax({
                            url         : settings.source.url,
                            method      : method,
                            contentType : contentType,
                            dataType    : dataType,
                            cache       : cache,
                            processData : processData,
                            data        : $formData,
            
                            beforeSend  : function(){
                                $btn.Preloader().start();
                            }
                        })
            
                        .done(function(response){
                            $btn.Preloader().stop();
            
                            // Sukces
                            if (in_array(response.status, statusAllowed)) {
                                if (response.modal == undefined) {
                                    cswal(swalTitle, successOperation, 'success').then(function(){
                                        if (toggleDialog == true && shwowDialog == true) $dialog.modal('hide');
                                        else reload();
                                    });
                                }

                                // Opcja dodatkowego modala
                                else {
                                    const $modal = response.modal;

                                    swal.fire({
                                        title   : $modal.title,
                                        text    : $modal.swal.text,
                                        type    : 'warning',
                                        
                                        showCancelButton    : true,
                                        cancelButtonText    : 'Anuluj',
                                        confirmButtonText   : $modal.swal.btn,

                                        allowOutsideClick : false
                                    }).then(function(result){
                                        if (result.value) {
                                            var $_axios = $().axios({ type : 'inputs' });

                                            $_axios.load({
                                                saveButton : {
                                                    name : '.btn-save-multi-modal-data'
                                                },
    
                                                dialog : {
                                                    name    : 'multi-dialog',
                                                    show    : true,
                                                    toggle  : true,
                                                    multi   : true,
                                                    zIndex  : 1100,
                                                    title   : $modal.title,
                                                },
    
                                                layout : {
                                                    columns : 2
                                                },
    
                                                source : {
                                                    url     : $modal.source.load.url,
                                                    data    : sourceDataMultiModal($modal.source.load.data),
                                                    done    : function(){
                                                        if ($modal.info != undefined) $('.multi-dialog').find('.GetAjaxDataForm').append('<span class="form-text text-muted" style="font-size:11px;margin-top:30px;max-width:600px">* ' + $modal.info + '</span>');
                                                    }
                                                }
                                            });

                                            $(document).on('click', '.btn-save-multi-modal-data', function(e){
                                                e.preventDefault();

                                                $_axios.save({
                                                    source : {
                                                        url     : $modal.source.save.url,
                                                        data    : $modal.source.save.data
                                                    },
                                        
                                                    swal : {
                                                        title   : $modal.title,
                                                        text    : 'Musisz uzupełnić wymagane pola!',
                                                        type    : 'error'
                                                    }
                                                });
                                            });
                                        }
                                    });
                                }

                                // Dodatkowe dzialania
                                if (settings.source.done && typeof(settings.source.done) === 'function') settings.source.done.call(this, response);
                            }

                            // Sesja wygasła
                            else if (response.status == (-10)) swal.fire('Twoja sesja wygasła!', 'Musisz ponownie się zalogować, aby kontynuuować operację.', 'error').then(function(){ location.reload() });
                            
                            // Błąd
                            else {
                                response.error == undefined ? swal.fire('Nieznany błąd!', unknowProblem, 'error') : swal.fire('Wystąpił błąd', response.error, 'error');
                            }
                        })

                        .fail(function(){
                            $btn.Preloader().stop();
                            swal.fire('Błąd połączenia!', navigator.onLine ? failConnect : failDisconnect, 'error');

                            // Dodatkowe dzialania
                            if (settings.source.fail && typeof(settings.source.fail) === 'function') settings.source.fail.call(this);
                        });
                    } else {
                        swal.fire(swalTitle, swalText, swalType);
                        if (shwowDialog == true) resetScrollabe($t.find('#GetAjaxData .kt-scroll'));
                    }
                }
            });
        }


        /**
         * 
         * Special calls methods.
         * 
         * @param object config
         * @return void
         * 
         * @package axios
         * @access public
         * 
         */
        const outside = {
            overwrite   : function(settings) { overwrite(settings) },
            load        : function(settings) { load(settings) },
            save        : function(settings) { save(settings) },
            params      : function(settings) { params(settings) },
            reload      : function() { reload() }
        }

        return outside;
    }
})(jQuery);