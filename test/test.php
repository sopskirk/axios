<?php
    // Prosty przyklad generowania pol formularza poprzez PHP.
    
    $topics = [
        'names' => [ 'aaa', 'bbb', 'ccc' ],
        'keys'  => [ 5, 1, 12 ]
    ];
    
    $result = [
        'status'    => true,
        'data'      => [
            'question' => [
                'label'     => 'Pytanie',
                'required'  => true,
                'widget'    => 'textarea'
            ],

            'answers' => [
                'label'         => 'Odpowiedzi',
                'required'      => true,
                'widget'        => 'textarea',
                'placeholder'   => 'Odpowiedzi oddzielone enterem'
            ],

            'good_answers' => [
                'label'         => 'Dobre odpowiedzi',
                'required'      => true,
                'widget'        => 'textarea',
                'placeholder'   => 'Odpowiedzi oddzielone enterem'
            ],

            'id_topic' => [
                'label'     => 'Temat',
                'select'    => $topics['names'],
                'keys'      => $topics['keys'],
                'widget'    => 'select'
            ],

            'time' => [
                'label'     => 'Czas [s]',
                'default'   => 120,
                'maxlength' => 3
            ],

            'points' => [
                'label'     => 'Punkty',
                'default'   => 3,
                'maxlength' => 1
            ],

            'active' => [
                'label'     => 'aktywny',
                'select'    => [ 'Nie', 'Tak' ],
                'keys'      => [ 0, 1 ],
                'widget'    => 'select',
                'default'   => 1
            ],

            'date_open' => [
                'label'     => 'Data otwarcia',
                'widget'    => 'Date',
                'viewMode'  => 'months',
                'default'   => date('Y-m-d')
            ]
        ]
    ];

    echo json_encode($result);