// Prosty przyklad pobrania pol formularza z serwera.

$(function(){
    const $axios = $().axios({ type : 'inputs' });

    $axios.load({
        saveButton : {
            name : '.btn-save-edit'
        },
        
        dialog : {
            title   : 'Edycja danych',
            show    : true,
            toggle  : false
        },
        
        layout : {
            columns : 2
        },
        
        source : {
            url : '/test/test.php'
        }
    });
    
    $(document).on('click', '.btn-save-edit', function(e){
        $axios.save({
            source : {
                url : '/test/test.php'
            }
        });
    });
});